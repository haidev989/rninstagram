import {firebase} from '@react-native-firebase/database';

export const firebaseConfig = {
  apiKey: 'AIzaSyANk74GAQlyQdUk9a8RiFUurEw-cR9piqE',
  authDomain: 'rninstagram-b2e03.firebaseapp.com',
  databaseURL:
    'https://rninstagram-b2e03-default-rtdb.asia-southeast1.firebasedatabase.app',
  projectId: 'rninstagram-b2e03',
  storageBucket: 'rninstagram-b2e03.appspot.com',
  messagingSenderId: '1062064725713',
  appId: '1:1062064725713:web:96c9e5c321a079b8996ff0',
  measurementId: 'G-2P3LQV3Q37',
};

export const firebaseApp = firebase
  .app()
  .database(
    'https://rninstagram-b2e03-default-rtdb.asia-southeast1.firebasedatabase.app/',
  );
