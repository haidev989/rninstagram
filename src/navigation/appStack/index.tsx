import React from 'react';
import PropTypes from 'prop-types';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ScreenName} from '../screenName';
import {OptionsShowHeader} from '../ConfigCommon';
import BottomTab from '../bottomTab';

const Stack = createNativeStackNavigator();
const AppStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={ScreenName.BOTTOM_SCREEN}
        component={BottomTab}
        options={OptionsShowHeader}
      />
    </Stack.Navigator>
  );
};

AppStack.propTypes = {};

export default AppStack;
