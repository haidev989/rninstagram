import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from '../../screens/home';
import {ScreenName} from '../screenName';
import ActivityScreen from '../../screens/activity';
import NewsScreen from '../../screens/news';
import SearchScreen from '../../screens/search';
import AccountScreen from '../../screens/account';
import {Image, View} from 'react-native';
import {Images} from '../../assets';
import {COLORS} from '../../utils/COLORS';

interface TabProps {
  source: String;
  color: String;
  size: number;
}
const Tab = createBottomTabNavigator();

const BottomTab = () => {
  const RenderIconTab = (props: TabProps) => {
    const {source, color, size} = props;
    return (
      <View>
        <Image
          source={source}
          resizeMode="contain"
          style={{
            tintColor: color,
            width: size,
            height: size,
          }}
        />
      </View>
    );
  };

  return (
    <Tab.Navigator
      initialRouteName={ScreenName.HOME_SCREEN}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, size}) => {
          let tabColor = focused ? COLORS.blue : COLORS.greyTab;
          let iconTab = Images.icHome;
          switch (route.name) {
            case ScreenName.HOME_SCREEN: {
              iconTab = Images.icHome;
              break;
            }
            case ScreenName.SEARCH_SCREEN: {
              iconTab = Images.icSearch;
              break;
            }
            case ScreenName.NEWS_SCREEN: {
              iconTab = Images.icVideo;
              break;
            }
            case ScreenName.ACTIVITY_SCREEN: {
              iconTab = Images.icHeart;
              break;
            }
            case ScreenName.ACTIVITY_SCREEN: {
              iconTab = Images.icAccount;
              break;
            }
          }
          return (
            <RenderIconTab source={iconTab} color={tabColor} size={size} />
          );
        },
      })}
      backBehavior="initialRoute">
      <Tab.Screen
        name={ScreenName.HOME_SCREEN}
        component={HomeScreen}
        options={{
          tabBarLabel: '',
        }}
      />
      <Tab.Screen
        name={ScreenName.SEARCH_SCREEN}
        component={SearchScreen}
        options={{
          tabBarLabel: '',
        }}
      />
      <Tab.Screen
        name={ScreenName.NEWS_SCREEN}
        component={NewsScreen}
        options={{
          tabBarLabel: '',
        }}
      />
      <Tab.Screen
        name={ScreenName.ACTIVITY_SCREEN}
        component={ActivityScreen}
        options={{
          tabBarLabel: '',
        }}
      />
      <Tab.Screen
        name={ScreenName.ACCOUNT_SCREEN}
        component={AccountScreen}
        options={{
          tabBarLabel: 'a',
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTab;
