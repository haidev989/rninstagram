import React from 'react';
import PropTypes from 'prop-types';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ScreenName, StackName} from './screenName';
import BottomTab from './bottomTab';
import {OptionsShowHeader} from './ConfigCommon';
import AuthStack from './authStack';
import AppStack from './appStack';
import {navigationRef} from './NavigationHelper';

const Stack = createNativeStackNavigator();

const MainNavigation = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName={StackName.AUTH_STACK}>
        <Stack.Screen
          name={StackName.AUTH_STACK}
          component={AuthStack}
          options={OptionsShowHeader}
        />
        <Stack.Screen
          name={StackName.APP_STACK}
          component={AppStack}
          options={OptionsShowHeader}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

MainNavigation.propTypes = {};

export default MainNavigation;
