import React from 'react';
import PropTypes from 'prop-types';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ScreenName} from '../screenName';
import LoginScreen from '../../screens/login';
import {OptionsShowHeader} from '../ConfigCommon';
import RegisterScreen from '../../screens/register';
import ForgotScreen from '../../screens/forgotPassword';

const Stack = createNativeStackNavigator();
const AuthStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={ScreenName.LOGIN_SCREEN}
        component={LoginScreen}
        options={OptionsShowHeader}
      />
      <Stack.Screen
        name={ScreenName.REGISTER_SCREEN}
        component={RegisterScreen}
        options={OptionsShowHeader}
      />
      <Stack.Screen
        name={ScreenName.FORGOT_SCREEN}
        component={ForgotScreen}
        options={OptionsShowHeader}
      />
    </Stack.Navigator>
  );
};

AuthStack.propTypes = {};

export default AuthStack;
