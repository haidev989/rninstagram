export const ScreenName = {
  HOME_SCREEN: 'HomeScreen',
  SEARCH_SCREEN: 'SearchScreen',
  NEWS_SCREEN: 'NewsScreen',
  ACTIVITY_SCREEN: 'ActivityScreen',
  ACCOUNT_SCREEN: 'AccountScreen',
  BOTTOM_SCREEN: 'BottomScreen',
  LOGIN_SCREEN: 'LoginScreen',
  REGISTER_SCREEN: 'RegisterScreen',
  FORGOT_SCREEN: 'ForgotScreen',
};

export const StackName = {
  MAIN_STACK: 'MainStack',
  APP_STACK: 'AppStack',
  AUTH_STACK: 'AuthStack'
}
