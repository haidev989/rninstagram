export const Images = {
  icHome: require('./Icon/icon_home.png'),
  icSearch: require('./Icon/icon_search.png'),
  icVideo: require('./Icon/icon_video.png'),
  icHeart: require('./Icon/icon_heart.png'),
  icAccount: require('./Icon/icon_account.png'),
};
