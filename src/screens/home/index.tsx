import React from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity, View} from 'react-native';
import {ScreenName} from '../../navigation/screenName';

function HomeScreen(props) {
  const gotoOtherScreen = () => {
    props.navigation.navigate(ScreenName.SEARCH_SCREEN);
  };
  return (
    <View>
      <TouchableOpacity onPress={gotoOtherScreen}>
        <Text>Home screen</Text>
      </TouchableOpacity>
    </View>
  );
}

HomeScreen.propTypes = {};

export default HomeScreen;
