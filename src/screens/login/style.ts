import {COLORS} from './../../utils/COLORS';
import {StyleSheet} from 'react-native';
import CommonStyle from '../../utils/CommonStyle';

export default StyleSheet.create({
  ...CommonStyle,
  body: {
    paddingHorizontal: 15,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  textInput: {
    borderWidth: 1,
    borderColor: COLORS.greyTab,
    paddingVertical: 10,
  },
  wrapperInput: {
    paddingVertical: 10,
  },
  wrapperTitle: {
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    color: COLORS.white,
  },
  loginBtn: {
    backgroundColor: COLORS.lightBlue,
    alignItems: 'center',
    borderRadius: 10,
    paddingHorizontal: 20,
  },
  wrapperTwoBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 30,
  },
  register: {
    color: 'black',
    paddingHorizontal: 20,
  },
  forgot: {
    color: 'black',
    paddingHorizontal: 20,
  },
  wrapperRegisterForgotPassword: {
    backgroundColor: COLORS.lightBlue,
    width: '45%',
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 5,
  },
  registerForgot: {
    color: COLORS.white,
    fontSize: 15,
  },
});
