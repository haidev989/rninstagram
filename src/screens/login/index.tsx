/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import styles from './style';
import {firebaseApp} from '../../firebase';
import {ScreenName, StackName} from '../../navigation/screenName';
import * as NavigationHelper from '../../navigation/NavigationHelper';

function LoginScreen(props: any) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = () => {
    const reference = firebaseApp.ref('/Users');
    reference.on('value', snapshot => {
      const users = snapshot.val();
      if (users.hasOwnProperty(username)) {
        NavigationHelper.reset(StackName.APP_STACK);
      } else {
        Alert.alert('Wrong username or password!');
      }
    });
  };

  const gotoRegister = () => {
    props.navigation.navigate(ScreenName.REGISTER_SCREEN);
  };

  const gotoForgotPassword = () => {
    props.navigation.navigate(ScreenName.FORGOT_SCREEN);
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.body}>
        <View style={styles.wrapperTitle}>
          <Text style={styles.title}>Login</Text>
        </View>
        <View style={styles.wrapperInput}>
          <Text>username</Text>
          <TextInput
            style={styles.textInput}
            value={username}
            onChangeText={text => setUsername(text)}
          />
        </View>
        <View style={styles.wrapperInput}>
          <Text>password</Text>
          <TextInput
            style={styles.textInput}
            value={password}
            onChangeText={text => setPassword(text)}
          />
        </View>
        <TouchableOpacity onPress={onSubmit} style={styles.loginBtn}>
          <Text style={styles.title}>Login</Text>
        </TouchableOpacity>
        <View style={styles.wrapperTwoBtn}>
          <TouchableOpacity
            onPress={gotoRegister}
            style={styles.wrapperRegisterForgotPassword}>
            <Text style={styles.registerForgot}>Register</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={gotoForgotPassword}
            style={styles.wrapperRegisterForgotPassword}>
            <Text style={styles.registerForgot}>Forgot password</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

LoginScreen.propTypes = {};

export default LoginScreen;
