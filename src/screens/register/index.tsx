import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {firebaseApp} from '../../firebase';
import styles from './style';
import Header from '../../components/header';

function RegisterScreen(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  let users;

  const onSubmitRegister = () => {
    const reference = firebaseApp.ref('/Users');
    reference.once('value', snapshot => {
      console.log(snapshot);

      users = snapshot.val();
      if (users.hasOwnProperty(username)) {
        Alert.alert('username ready exist!');
      } else {
        addDataBase(username);
      }
    });
  };

  const addDataBase = (id: string) => {
    firebaseApp.ref(`/Users/${id}`).set(
      {
        username,
        password,
      },
      function (error) {
        if (error) {
          // The write failed...
          Alert.alert('Lỗi');
        } else {
          // Data saved successfully!
          Alert.alert('Thành Công !!!');
        }
      },
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.body}>
        <Header title="Register" />

        <View style={styles.wrapperInput}>
          <Text>username</Text>
          <TextInput
            style={styles.textInput}
            value={username}
            onChangeText={text => setUsername(text)}
          />
        </View>
        <View style={styles.wrapperInput}>
          <Text>password</Text>
          <TextInput
            style={styles.textInput}
            value={password}
            onChangeText={text => setPassword(text)}
          />
        </View>
        <View style={styles.wrapperInput}>
          <Text>password again</Text>
          <TextInput
            style={styles.textInput}
            value={passwordConfirm}
            onChangeText={text => setPasswordConfirm(text)}
          />
        </View>
        <TouchableOpacity
          onPress={onSubmitRegister}
          style={styles.loginRegisterBtn}>
          <Text style={styles.title}>Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

RegisterScreen.propTypes = {};

export default RegisterScreen;
