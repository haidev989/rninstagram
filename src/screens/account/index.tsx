import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Alert, Image, Text, TouchableOpacity, View} from 'react-native';
import styles from './style';
import {Images} from '../../assets';
import {launchCamera} from 'react-native-image-picker';

function AccountScreen(props) {
  const [filePath, setFilePath] = useState({});

  const openPicker = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 200,
      maxWidth: 200,
    };
    try {
      launchCamera(options, response => {
        // Use launchImageLibrary to open image gallery
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = {uri: response.uri};
          setFilePath(source);
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };

          console.log(source);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };
  console.log({filePath});

  return (
    <View style={styles.containerMenu}>
      <TouchableOpacity onPress={openPicker}>
        <Image source={Images.icHeart} style={styles.avatar} />
      </TouchableOpacity>

      <Image source={filePath} style={styles.avatar} />

      <Text>Account screen</Text>
    </View>
  );
}

AccountScreen.propTypes = {};

export default AccountScreen;
