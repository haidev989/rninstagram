import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerMenu: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
});
