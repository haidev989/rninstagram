export const COLORS = {
  yellow: 'yellow',
  red: 'red',
  blue: 'blue',
  greyTab: '#ddd',
  greyTabFocused: '#000',
  white: 'white',
  lightBlue: '#add8e6',
  black: 'black',
};
