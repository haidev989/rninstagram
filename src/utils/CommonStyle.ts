import {COLORS} from './COLORS';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  loginRegisterBtn: {
    backgroundColor: COLORS.lightBlue,
    alignItems: 'center',
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
});
