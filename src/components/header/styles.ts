import {StyleSheet} from 'react-native';
import {COLORS} from '../../utils/COLORS';
import CommonStyle from '../../utils/CommonStyle';

export default StyleSheet.create({
  ...CommonStyle,
  wrapperHeader: {
    alignItems: 'center',
  },
  titleHeader: {
    color: COLORS.black,
    fontSize: 30,
  },
});
