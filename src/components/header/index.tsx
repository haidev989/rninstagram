import React from 'react';
import {Text, View} from 'react-native';
import styles from './styles';

interface Props {
  title: string;
}

const Header = (props: Props) => {
  return (
    <View style={styles.wrapperHeader}>
      <Text style={styles.titleHeader}>{props.title}</Text>
    </View>
  );
};

export default Header;
